# drmind

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## DRMIND.LSM

<table>
<tr><td>title</td><td>Dr. Mind</td></tr>
<tr><td>version</td><td>1.0</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-08-30</td></tr>
<tr><td>description</td><td>PC adaptation of a well-known board game.</td></tr>
<tr><td>summary</td><td>Dr. Mind is a PC adaptation of a well-known board game. The computer generates a secret color code, and the player has to figure out the exact pattern within 8 turns. After each turn, the computer provides clues about how many colors matched the solution. The game features a set of 24 pictures that are initially blurred - every time you work out a valid code combination, you are rewarded with a picture. This game is published under the CC BY-ND 4.0 license, which is roughly equivalent to "freeware with source code".</td></tr>
<tr><td>keywords</td><td>mind,mastermind,games</td></tr>
<tr><td>author</td><td>Mateusz Viste</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mateusz Viste</td></tr>
<tr><td>primary&nbsp;site</td><td>http://drmind.sourceforge.net</td></tr>
<tr><td>original&nbsp;site</td><td>http://drmind.sourceforge.net</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Creative Commons Attribution-NoDerivatives 4.0 International Public License](LICENSE)</td></tr>
</table>
